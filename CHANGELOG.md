
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:34PM

See merge request itentialopensource/adapters/adapter-akamai_gtm!17

---

## 0.4.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-akamai_gtm!15

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:48PM

See merge request itentialopensource/adapters/adapter-akamai_gtm!14

---

## 0.4.2 [08-07-2024]

* Changes made at 2024.08.06_20:02PM

See merge request itentialopensource/adapters/adapter-akamai_gtm!13

---

## 0.4.1 [08-05-2024]

* Changes made at 2024.08.05_18:19PM

See merge request itentialopensource/adapters/adapter-akamai_gtm!12

---

## 0.4.0 [07-03-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!11

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:17PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!10

---

## 0.3.2 [03-12-2024]

* Changes made at 2024.03.12_11:00AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!9

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:35AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!8

---

## 0.3.0 [01-09-2024]

* Adapter Migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!7

---

## 0.2.0 [06-02-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!5

---

## 0.1.5 [03-30-2022]

- Migration and fix for genericAdapterRequest

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!4

---

## 0.1.4 [04-30-2021]

- This patch fixes the paths for auth URLs used by the edge grid auth string generator. All affected paths should now properly be nested inside a /domains/ path.

See merge request itentialopensource/adapters/certified-integration-documents/telemetry-analytics/adapter-akamai_gtm!3

---

## 0.1.3 [04-26-2021]

- Update most of the paths in the adapter to include domains

See merge request itentialopensource/adapters/certified-integration-documents/telemetry-analytics/adapter-akamai_gtm!2

---

## 0.1.2 [04-12-2021]

- Added attach query params to url

See merge request itentialopensource/adapters/certified-integration-documents/telemetry-analytics/adapter-akamai_gtm!1

---

## 0.1.1 [04-02-2021]

- Initial Commit

See commit 589b8b4

---
