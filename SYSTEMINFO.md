# Akamai GTM

Vendor: Akamai
Homepage: https://www.akamai.com/

Product: Global Traffic Management
Product Page: https://www.akamai.com/products/global-traffic-management

## Introduction
We classify Akamai GTM into the Service Assurance domain as Akamai GTM provides an Internet-centric approach to ensure the availability, reliability, and performance of services.

"Global Traffic Management (GTM) is a cloud-based intelligent traffic manager that supports business continuity and your growing user base and uses load balancing to manage website and mobile performance demands"

## Why Integrate
The Akamai GTM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Akamai Global Traffic Management. With this adapter you have the ability to perform operations such as:

- Get AS-Map
- Get CIDR-Map
- Get Data Center
- Get, and Create Domain
- Get, and Create Resource
- Get Geographic-Map

## Additional Product Documentation
The [API documents for Akamai GTM](https://techdocs.akamai.com/gtm/reference/api)