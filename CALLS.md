## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Akamai GTM. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Akamai GTM.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Akamai_gtm. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameAsMaps(domainName, callback)</td>
    <td style="padding:15px">List AS maps: Returns a list of AS maps.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/as-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameAsMapsMapName(mapName, domainName, callback)</td>
    <td style="padding:15px">Get an AS map: Returns information for a specific AS map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/as-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDomainNameAsMapsMapName(mapName, domainName, body, callback)</td>
    <td style="padding:15px">Create or update an AS map: Run this operation to create or
update an AS map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/as-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNameAsMapsMapName(mapName, domainName, callback)</td>
    <td style="padding:15px">Remove an AS map: Removes a specific AS map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/as-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameCidrMaps(domainName, callback)</td>
    <td style="padding:15px">List CIDR maps: Returns a list of CIDR maps.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/cidr-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameCidrMapsMapName(mapName, domainName, callback)</td>
    <td style="padding:15px">Get a CIDR map: Returns information for a specific CIDR map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/cidr-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDomainNameCidrMapsMapName(mapName, domainName, body, callback)</td>
    <td style="padding:15px">Create or update a CIDR map: Run this operation to create or update a CIDR map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/cidr-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNameCidrMapsMapName(mapName, domainName, callback)</td>
    <td style="padding:15px">Remove a CIDR map: Removes a specific CIDR map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/cidr-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameDatacenters(domainName, callback)</td>
    <td style="padding:15px">List data centers: Returns a list of data centers.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDomainNameDatacenters(domainName, body, callback)</td>
    <td style="padding:15px">Create a data center: Creates a specific data center.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameDatacentersDatacenterId(datacenterId, domainName, callback)</td>
    <td style="padding:15px">Get a data center: Returns information for a specific data center.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDomainNameDatacentersDatacenterId(datacenterId, domainName, body, callback)</td>
    <td style="padding:15px">Update a data center: Updates a specific data center.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNameDatacentersDatacenterId(datacenterId, domainName, callback)</td>
    <td style="padding:15px">Remove a data center: Removes a data center. Removes a data
center, which is only possible when no</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDomainNameDatacentersDefaultDatacenterForMaps(domainName, callback)</td>
    <td style="padding:15px">Create a default data center: Creates a data center which can be used by map-type properties (CIDR,</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters/default-datacenter-for-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDomainNameDatacentersDatacenterForIpVersionSelectorIpv4(domainName, callback)</td>
    <td style="padding:15px">Create an IPv4 datacenter for ip-version-selector: Creates a data center which is solely reserved f</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters/datacenter-for-ip-version-selector-ipv4?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDomainNameDatacentersDatacenterForIpVersionSelectorIpv6(domainName, callback)</td>
    <td style="padding:15px">Create an IPv6 datacenter for ip-version-selector: Creates a data center which is solely reserved f</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/datacenters/datacenter-for-ip-version-selector-ipv6?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDomains(body, contractId, gid, callback)</td>
    <td style="padding:15px">Create a domain: Creates a new GTM domain.</td>
    <td style="padding:15px">{base_path}/{version}/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomains(callback)</td>
    <td style="padding:15px">List domains: Returns a list of GTM domains under your contract.</td>
    <td style="padding:15px">{base_path}/{version}/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainsDomainName(domainName, callback)</td>
    <td style="padding:15px">Get a domain: Returns information for a specific GTM domain.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDomainsDomainName(domainName, body, contractId, gid, callback)</td>
    <td style="padding:15px">Update a domain: Updates a specific GTM domain.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameGeographicMaps(domainName, callback)</td>
    <td style="padding:15px">List geographic maps: Returns a list of geographic maps.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/geographic-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameGeographicMapsMapName(mapName, domainName, callback)</td>
    <td style="padding:15px">Get a geographic map: Returns information for a specific geographic map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/geographic-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDomainNameGeographicMapsMapName(mapName, domainName, body, callback)</td>
    <td style="padding:15px">Create or update a geographic map: Run this operation to
create or update a geographic map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/geographic-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNameGeographicMapsMapName(mapName, domainName, callback)</td>
    <td style="padding:15px">Remove a geographic map: Removes a geographic map.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/geographic-maps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentity(callback)</td>
    <td style="padding:15px">Get identity: Returns information about the API client. Run this
operation for details on the API c</td>
    <td style="padding:15px">{base_path}/{version}/identity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityContracts(callback)</td>
    <td style="padding:15px">List contracts: Returns a list of API client `contracts`. When
you [Create a domain](#postdomains),</td>
    <td style="padding:15px">{base_path}/{version}/identity/contracts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdentityGroups(callback)</td>
    <td style="padding:15px">List groups: Returns a list of API client `groups` and their
parameters.  When you [Create a domain</td>
    <td style="padding:15px">{base_path}/{version}/identity/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameProperties(domainName, callback)</td>
    <td style="padding:15px">List properties: Returns a list of GTM properties.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNamePropertiesPropertyName(propertyName, domainName, callback)</td>
    <td style="padding:15px">Get a property: Returns information for a specific GTM property.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDomainNamePropertiesPropertyName(propertyName, domainName, body, callback)</td>
    <td style="padding:15px">Create or update a property: Run this operation to create or
update a GTM property.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNamePropertiesPropertyName(propertyName, domainName, callback)</td>
    <td style="padding:15px">Remove a property: Removes a specific GTM property.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/properties/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameResources(domainName, callback)</td>
    <td style="padding:15px">List resources: Returns a list of resources created within the
domain that constrain one or more pr</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameResourcesResourceName(resourceName, domainName, callback)</td>
    <td style="padding:15px">Get a resource: Returns information for a specific resource.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/resources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDomainNameResourcesResourceName(resourceName, domainName, body, callback)</td>
    <td style="padding:15px">Create or update a resource: Run this operation to create or
update a resource.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/resources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDomainNameResourcesResourceName(resourceName, domainName, callback)</td>
    <td style="padding:15px">Remove a resource: Removes a specific resource.</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/resources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainNameStatusCurrent(domainName, callback)</td>
    <td style="padding:15px">Get current status: Returns information about the current condition of the
change in the domain's a</td>
    <td style="padding:15px">{base_path}/{version}/domains/{pathv1}/status/current?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRunProposedPropertyLivenessTests(body, callback)</td>
    <td style="padding:15px">Run proposed liveness tests: Runs liveness test(s) contained by
property and returns the results.</td>
    <td style="padding:15px">{base_path}/{version}/run-proposed-property-liveness-tests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
