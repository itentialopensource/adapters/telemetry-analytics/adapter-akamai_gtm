
## 0.2.0 [06-02-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!5

---

## 0.1.5 [03-30-2022]

- Migration and fix for genericAdapterRequest

See merge request itentialopensource/adapters/telemetry-analytics/adapter-akamai_gtm!4

---

## 0.1.4 [04-30-2021]

- This patch fixes the paths for auth URLs used by the edge grid auth string generator. All affected paths should now properly be nested inside a /domains/ path.

See merge request itentialopensource/adapters/certified-integration-documents/telemetry-analytics/adapter-akamai_gtm!3

---

## 0.1.3 [04-26-2021]

- Update most of the paths in the adapter to include domains

See merge request itentialopensource/adapters/certified-integration-documents/telemetry-analytics/adapter-akamai_gtm!2

---

## 0.1.2 [04-12-2021]

- Added attach query params to url

See merge request itentialopensource/adapters/certified-integration-documents/telemetry-analytics/adapter-akamai_gtm!1

---

## 0.1.1 [04-02-2021]

- Initial Commit

See commit 589b8b4

---
