## Authenticating Akamai GTM Adapter 

This document will go through the steps for authenticating the Akamai GTM with Akamai EdgeGrid Authentication. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

Companies periodically change authentication methods to provide better security. As this happens this section should be updated and contributed/merge back into the adapter repository.

### Akamai EdgeGrid Authentication
The Akamai GTM adapter requires Akamai EdgeGrid Authentication. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Akamai GTM server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. In order to use Akamai EdgeGrid Authentication, users need to first create authentication credentials including client_secret, host, access_token, and client_token and copy them to the adapter service config. Follow the steps to create an authentication credential: https://techdocs.akamai.com/developer/docs/set-up-authentication-credentials
4. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "no_authentication",
  "auth_logging": false,
  "clientToken": "clientToken",
  "clientSecret": "clientSecret",
  "accessToken": "accessToken",
}
```
you can leave all of the other properties in the authentication section, they will not be used for Akamai EdgeGrid authentication.

4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct client_secret, access_token, and client_token.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
