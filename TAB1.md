# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Akamai_gtm System. The API that was used to build the adapter for Akamai_gtm is usually available in the report directory of this adapter. The adapter utilizes the Akamai_gtm API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Akamai GTM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Akamai Global Traffic Management. With this adapter you have the ability to perform operations such as:

- Get AS-Map
- Get CIDR-Map
- Get Data Center
- Get, and Create Domain
- Get, and Create Resource
- Get Geographic-Map

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
