/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-akamai_gtm',
      type: 'AkamaiGtm',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AkamaiGtm = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Akamai_gtm Adapter Test', () => {
  describe('AkamaiGtm Class Tests', () => {
    const a = new AkamaiGtm(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-akamai_gtm-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-akamai_gtm-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const autonomousSystemMapsDomainName = 'fakedata';
    describe('#getDomainNameAsMaps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDomainNameAsMaps(autonomousSystemMapsDomainName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-akamai_gtm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AutonomousSystemMaps', 'getDomainNameAsMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const autonomousSystemMapsMapName = 'fakedata';
    const autonomousSystemMapsPutDomainNameAsMapsMapNameBodyParam = {
      assignments: [
        {}
      ],
      defaultDatacenter: {},
      name: 'string'
    };
    describe('#putDomainNameAsMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDomainNameAsMapsMapName(autonomousSystemMapsMapName, autonomousSystemMapsDomainName, autonomousSystemMapsPutDomainNameAsMapsMapNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AutonomousSystemMaps', 'putDomainNameAsMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameAsMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameAsMapsMapName(autonomousSystemMapsMapName, autonomousSystemMapsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AutonomousSystemMaps', 'getDomainNameAsMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cIDRMapsDomainName = 'fakedata';
    describe('#getDomainNameCidrMaps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameCidrMaps(cIDRMapsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CIDRMaps', 'getDomainNameCidrMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cIDRMapsMapName = 'fakedata';
    const cIDRMapsPutDomainNameCidrMapsMapNameBodyParam = {
      name: 'string',
      assignments: [
        {
          datacenterId: 3,
          nickname: 'string',
          blocks: [
            'string'
          ]
        }
      ],
      defaultDatacenter: {
        datacenterId: 10,
        nickname: 'string'
      },
      links: [
        null
      ]
    };
    describe('#putDomainNameCidrMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDomainNameCidrMapsMapName(cIDRMapsMapName, cIDRMapsDomainName, cIDRMapsPutDomainNameCidrMapsMapNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CIDRMaps', 'putDomainNameCidrMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameCidrMapsMapName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDomainNameCidrMapsMapName(cIDRMapsMapName, cIDRMapsDomainName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-akamai_gtm-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CIDRMaps', 'getDomainNameCidrMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataCentersDomainName = 'fakedata';
    const dataCentersPostDomainNameDatacentersBodyParam = {
      datacenterId: 8
    };
    describe('#postDomainNameDatacenters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDomainNameDatacenters(dataCentersDomainName, dataCentersPostDomainNameDatacentersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'postDomainNameDatacenters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDomainNameDatacentersDatacenterForIpVersionSelectorIpv4 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDomainNameDatacentersDatacenterForIpVersionSelectorIpv4(dataCentersDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'postDomainNameDatacentersDatacenterForIpVersionSelectorIpv4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDomainNameDatacentersDatacenterForIpVersionSelectorIpv6 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDomainNameDatacentersDatacenterForIpVersionSelectorIpv6(dataCentersDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'postDomainNameDatacentersDatacenterForIpVersionSelectorIpv6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDomainNameDatacentersDefaultDatacenterForMaps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDomainNameDatacentersDefaultDatacenterForMaps(dataCentersDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'postDomainNameDatacentersDefaultDatacenterForMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameDatacenters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameDatacenters(dataCentersDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'getDomainNameDatacenters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dataCentersDatacenterId = 555;
    const dataCentersPutDomainNameDatacentersDatacenterIdBodyParam = {
      datacenterId: 9
    };
    describe('#putDomainNameDatacentersDatacenterId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDomainNameDatacentersDatacenterId(dataCentersDatacenterId, dataCentersDomainName, dataCentersPutDomainNameDatacentersDatacenterIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'putDomainNameDatacentersDatacenterId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameDatacentersDatacenterId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameDatacentersDatacenterId(dataCentersDatacenterId, dataCentersDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Downpatrick', data.response.city);
                assert.equal(0, data.response.cloneOf);
                assert.equal('EU', data.response.continent);
                assert.equal('GB', data.response.country);
                assert.equal(3133, data.response.datacenterId);
                assert.equal(54.367, data.response.latitude);
                assert.equal(-5.582, data.response.longitude);
                assert.equal('Winterfell', data.response.nickname);
                assert.equal(true, data.response.virtual);
                assert.equal('object', typeof data.response.defaultLoadObject);
                assert.equal(true, Array.isArray(data.response.links));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'getDomainNameDatacentersDatacenterId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const domainsPostDomainsBodyParam = {
      cidrMaps: [
        {}
      ],
      datacenters: [
        null
      ],
      defaultErrorPenalty: 5,
      defaultSslClientCertificate: 'string',
      defaultSslClientPrivateKey: 'string',
      defaultTimeoutPenalty: 1,
      emailNotificationList: [
        'string'
      ],
      lastModified: 'string',
      lastModifiedBy: 'string',
      loadFeedback: true,
      loadImbalancePercentage: 10,
      modificationComments: 'string',
      name: 'string',
      properties: [
        null
      ],
      resources: [
        null
      ],
      type: 'full',
      geographicMaps: [
        null
      ]
    };
    describe('#postDomains - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDomains(domainsPostDomainsBodyParam, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.resource);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domains', 'postDomains', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomains - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomains((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domains', 'getDomains', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const domainsDomainName = 'fakedata';
    const domainsPutDomainsDomainNameBodyParam = {
      cidrMaps: [
        {}
      ],
      datacenters: [
        null
      ],
      defaultErrorPenalty: 2,
      defaultSslClientCertificate: 'string',
      defaultSslClientPrivateKey: 'string',
      defaultTimeoutPenalty: 5,
      emailNotificationList: [
        'string'
      ],
      lastModified: 'string',
      lastModifiedBy: 'string',
      loadFeedback: true,
      loadImbalancePercentage: 4,
      modificationComments: 'string',
      name: 'string',
      properties: [
        null
      ],
      resources: [
        null
      ],
      type: 'weighted',
      geographicMaps: [
        null
      ]
    };
    describe('#putDomainsDomainName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDomainsDomainName(domainsDomainName, domainsPutDomainsDomainNameBodyParam, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domains', 'putDomainsDomainName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainsDomainName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainsDomainName(domainsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2014-04-08T18:25:51.000+0000', data.response.lastModified);
                assert.equal('admin@example.com', data.response.lastModifiedBy);
                assert.equal(true, data.response.loadFeedback);
                assert.equal(10, data.response.loadImbalancePercentage);
                assert.equal('CIDRMap example', data.response.modificationComments);
                assert.equal('example.akadns.net', data.response.name);
                assert.equal('full', data.response.type);
                assert.equal(true, Array.isArray(data.response.emailNotificationList));
                assert.equal('object', typeof data.response.status);
                assert.equal(true, Array.isArray(data.response.links));
                assert.equal(true, Array.isArray(data.response.cidrMaps));
                assert.equal(true, Array.isArray(data.response.datacenters));
                assert.equal(true, Array.isArray(data.response.geographicMaps));
                assert.equal(true, Array.isArray(data.response.properties));
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Domains', 'getDomainsDomainName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const geographicMapsDomainName = 'fakedata';
    describe('#getDomainNameGeographicMaps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameGeographicMaps(geographicMapsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeographicMaps', 'getDomainNameGeographicMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const geographicMapsMapName = 'fakedata';
    const geographicMapsPutDomainNameGeographicMapsMapNameBodyParam = {
      name: 'string',
      defaultDatacenter: {
        datacenterId: 9,
        nickname: 'string'
      },
      assignments: [
        {
          datacenterId: 7,
          nickname: 'string',
          countries: [
            'string'
          ]
        }
      ],
      links: [
        null
      ]
    };
    describe('#putDomainNameGeographicMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDomainNameGeographicMapsMapName(geographicMapsMapName, geographicMapsDomainName, geographicMapsPutDomainNameGeographicMapsMapNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeographicMaps', 'putDomainNameGeographicMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameGeographicMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameGeographicMapsMapName(geographicMapsMapName, geographicMapsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UK Delivery', data.response.name);
                assert.equal('object', typeof data.response.defaultDatacenter);
                assert.equal(true, Array.isArray(data.response.assignments));
                assert.equal(true, Array.isArray(data.response.links));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeographicMaps', 'getDomainNameGeographicMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentity((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1-1TJZFB', data.response.accountId);
                assert.equal('ed@example.com', data.response.userName);
                assert.equal('-05:00', data.response.userTimeZone);
                assert.equal('en_US', data.response.locale);
                assert.equal('Eddard', data.response.firstName);
                assert.equal('Stark', data.response.lastName);
                assert.equal('ed@example.com', data.response.email);
                assert.equal(true, data.response.active);
                assert.equal(true, Array.isArray(data.response.contracts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'getIdentity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityContracts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityContracts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1-1TJZFB', data.response.accountId);
                assert.equal(true, Array.isArray(data.response.contracts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'getIdentityContracts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdentityGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdentityGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.groups));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'getIdentityGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const propertiesDomainName = 'fakedata';
    describe('#getDomainNameProperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameProperties(propertiesDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Properties', 'getDomainNameProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const propertiesPropertyName = 'fakedata';
    const propertiesPutDomainNamePropertiesPropertyNameBodyParam = {
      backupCName: 'string',
      backupIp: 'string',
      balanceByDownloadScore: false,
      cname: 'string',
      dynamicTTL: 8,
      failbackDelay: 5,
      failoverDelay: 8,
      handoutMode: 'all-live-ips',
      healthMax: 10,
      healthMultiplier: 7,
      healthThreshold: 7,
      ipv6: false,
      livenessTests: [
        {
          name: 'string'
        }
      ],
      loadImbalancePercentage: 5,
      mapName: 'string',
      maxUnreachablePenalty: 5,
      mxRecords: [
        {}
      ],
      name: 'string',
      scoreAggregationType: 'median',
      staticTTL: 5,
      stickinessBonusPercentage: 7,
      stickinessBonusConstant: 8,
      trafficTargets: [
        {
          name: 'string'
        }
      ],
      type: 'weighted-round-robin-load-feedback',
      unreachableThreshold: 9,
      useComputedTargets: true,
      lastModified: 'string'
    };
    describe('#putDomainNamePropertiesPropertyName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDomainNamePropertiesPropertyName(propertiesPropertyName, propertiesDomainName, propertiesPutDomainNamePropertiesPropertyNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Properties', 'putDomainNamePropertiesPropertyName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNamePropertiesPropertyName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNamePropertiesPropertyName(propertiesPropertyName, propertiesDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.balanceByDownloadScore);
                assert.equal(300, data.response.dynamicTTL);
                assert.equal(0, data.response.failbackDelay);
                assert.equal(0, data.response.failoverDelay);
                assert.equal('normal', data.response.handoutMode);
                assert.equal(false, data.response.ipv6);
                assert.equal('2014-04-08T15:11:10.000+0000', data.response.lastModified);
                assert.equal('www', data.response.name);
                assert.equal('mean', data.response.scoreAggregationType);
                assert.equal(600, data.response.staticTTL);
                assert.equal(0, data.response.stickinessBonusConstant);
                assert.equal(0, data.response.stickinessBonusPercentage);
                assert.equal('failover', data.response.type);
                assert.equal(false, data.response.useComputedTargets);
                assert.equal(true, Array.isArray(data.response.mxRecords));
                assert.equal(true, Array.isArray(data.response.links));
                assert.equal(true, Array.isArray(data.response.livenessTests));
                assert.equal(true, Array.isArray(data.response.trafficTargets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Properties', 'getDomainNamePropertiesPropertyName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourcesDomainName = 'fakedata';
    describe('#getDomainNameResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameResources(resourcesDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Resources', 'getDomainNameResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resourcesResourceName = 'fakedata';
    const resourcesPutDomainNameResourcesResourceNameBodyParam = {
      aggregationType: 'sum',
      name: 'string',
      type: 'Non-XML load object via HTTPS'
    };
    describe('#putDomainNameResourcesResourceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDomainNameResourcesResourceName(resourcesResourceName, resourcesDomainName, resourcesPutDomainNameResourcesResourceNameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Resources', 'putDomainNameResourcesResourceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainNameResourcesResourceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameResourcesResourceName(resourcesResourceName, resourcesDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('latest', data.response.aggregationType);
                assert.equal('**', data.response.constrainedProperty);
                assert.equal('Supply levels of Arbor Gold', data.response.description);
                assert.equal('arbor-gold', data.response.name);
                assert.equal('Non-XML load object via HTTP', data.response.type);
                assert.equal(0, data.response.upperBound);
                assert.equal(true, Array.isArray(data.response.links));
                assert.equal(true, Array.isArray(data.response.resourceInstances));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Resources', 'getDomainNameResourcesResourceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusDomainName = 'fakedata';
    describe('#getDomainNameStatusCurrent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainNameStatusCurrent(statusDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('5beb11ae-8908-4bfe-8459-e88efc4d2fdc', data.response.changeId);
                assert.equal('Current configuration has been propagated to all GTM name servers.', data.response.message);
                assert.equal(true, data.response.passingValidation);
                assert.equal('COMPLETE', data.response.propagationStatus);
                assert.equal('2014-03-08T15:39:00.000+0000', data.response.propagationStatusDate);
                assert.equal(true, Array.isArray(data.response.links));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'getDomainNameStatusCurrent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const toolsPostRunProposedPropertyLivenessTestsBodyParam = {
      domainName: 'string',
      property: {}
    };
    describe('#postRunProposedPropertyLivenessTests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRunProposedPropertyLivenessTests(toolsPostRunProposedPropertyLivenessTestsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Datacenter "Cambridge office (physical datacenterId 3200)", server IP 1.2.3.5, test "test object 1": liveness test failed: Connection timed out\nDatacenter "Cambridge office (physical datacenterId 3200)", server IP 1.2.3.4, test "test object 1": liveness test failed: Connection timed out\nDatacenter "Cambridge office (physical datacenterId 3200)", server IP 1.2.3.5, test "test object 2": liveness test failed: Connection timed out\nDatacenter "Cambridge office (physical datacenterId 3200)", server IP 1.2.3.4, test "test object 2": liveness test failed: Connection timed out\nDatacenter "San Mateo office (physical datacenterId 3201)", server IP 1.2.3.7, test "test object 1": liveness test failed: Connection timed out\nDatacenter "San Mateo office (physical datacenterId 3201)", server IP 1.2.3.6, test "test object 1": liveness test failed: Connection timed out\nDatacenter "San Mateo office (physical datacenterId 3201)", server IP 1.2.3.7, test "test object 2": liveness test failed: Connection timed out\nDatacenter "San Mateo office (physical datacenterId 3201)", server IP 1.2.3.6, test "test object 2": liveness test failed: Connection timed out\n', data.response.result);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tools', 'postRunProposedPropertyLivenessTests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNameAsMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDomainNameAsMapsMapName(autonomousSystemMapsMapName, autonomousSystemMapsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AutonomousSystemMaps', 'deleteDomainNameAsMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNameCidrMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDomainNameCidrMapsMapName(cIDRMapsMapName, cIDRMapsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CIDRMaps', 'deleteDomainNameCidrMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNameDatacentersDatacenterId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDomainNameDatacentersDatacenterId(dataCentersDatacenterId, dataCentersDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataCenters', 'deleteDomainNameDatacentersDatacenterId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNameGeographicMapsMapName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDomainNameGeographicMapsMapName(geographicMapsMapName, geographicMapsDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeographicMaps', 'deleteDomainNameGeographicMapsMapName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNamePropertiesPropertyName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDomainNamePropertiesPropertyName(propertiesPropertyName, propertiesDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Properties', 'deleteDomainNamePropertiesPropertyName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDomainNameResourcesResourceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDomainNameResourcesResourceName(resourcesResourceName, resourcesDomainName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Resources', 'deleteDomainNameResourcesResourceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
